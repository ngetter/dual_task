# import pygame._view
from pygame import *
import pygame
import sys
from pygame.locals import *
import os

def loadImage(name, colorkey = None):
    try:
        image = pygame.image.load(name)
        image = image.convert_alpha()

        return image, image.get_rect()

    except pygame.error:
        print ('Cannot load image:', name)


class Plane(pygame.sprite.Sprite):
    def __init__(self,subjectNo,sessionNo):
        pygame.sprite.Sprite.__init__(self)
        self.image, self.rect = loadImage('sprite.png', -1)
        pygame.transform.scale(self.image, (8,8))
        #self.rect = self.image.get_rect()
        screen = pygame.display.get_surface()
        self.area = screen.get_rect()
        self.STEP = 9
        self.speed = 200 #pixels per second
        self.MARGIN = self.area.w * 0.1
        self.xstep = self.STEP
        self.ystep = 0
        self.degrees = 0
        self.initialupdate = 0
        #time to stop task = #of frames x frame time
        self.timetostop = 180000
        self.direction = 'right'
        self.rect = self.image.get_rect(centerx = 512,
                     centery = self.area.h * 0.45)
        self.subjectNo = subjectNo
        self.sessionNo = sessionNo

        filename = 'sprite_%s_%s.txt'%(subjectNo,sessionNo)
        i = 0 
        while os.path.exists(filename):
            i+=1
            filename = 'sprite_%s_%s(%s).txt'%(subjectNo,sessionNo,i)
            print(filename)
        self.log = open(filename,'w')
            
    def update(self, time_passed):
        if (pygame.time.get_ticks() - self.initialupdate) >= self.timetostop  and self.sessionNo == 1:
            pygame.quit()
            sys.exit()

        if self.initialupdate == 0:
            self.STEP = 0
            self.initialupdate = pygame.time.get_ticks()
        else:
            self.STEP = (time_passed / 1000.0) * self.speed
			
        if self.degrees:
            self._spin()
            self._move()
        else:
            self._move()

    def _move(self):

        if  self.direction == 'left':
            self.xstep = -self.STEP
        elif self.direction == 'right':
            self.xstep = self.STEP

        
        newpos = self.rect.move((self.xstep, self.ystep))
        self.log.write('%s,%s\n'%(str(newpos.centerx),str(pygame.time.get_ticks())))
        
        if self.direction == 'right' and self.rect.right > self.area.right - self.MARGIN:
            self.turn('left')

        if self.direction == 'left' and self.rect.left < self.area.left + self.MARGIN:
            self.turn('right')


        self.rect = newpos

    def _spin(self):
        center = self.rect.center
        self.degrees = self.degrees + 12
        if self.degrees >= 360:
            self.degrees = 0
            self.image = self.original
        else:
            self.image = pygame.transform.rotate(self.original, self.degrees)

        self.rect = self.image.get_rect(center=center)


    def hit(self):
        if not self.degrees:
            self.degrees = 1
            self.original = self.image

    def turn(self, direction):

        if direction == 'right' and self.direction == 'left':
            self.xstep = self.STEP
            self.ystep = 0
            self.direction = 'right'
            self.image = pygame.transform.flip(self.image, True, False)

        elif direction == 'left' and self.direction == 'right':
            self.xstep = -self.STEP
            self.ystep = 0
            self.direction = 'left'
            self.image = pygame.transform.flip(self.image, True, False)

    def inPosition(self, test):
        return self.rect.colliderect(test)
