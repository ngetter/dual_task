# import pygame._view
import pygame
from pygame import *
def loadImage(name, colorkey = None):
    try:
        image = pygame.image.load(name)
        image = image.convert_alpha()

        return image, image.get_rect()

    except pygame.error:
        print ('Cannot load image:', name)

class signalLight(pygame.sprite.Sprite):
    def __init__(self):
        pygame.sprite.Sprite.__init__(self)
        self.redLight, _ = loadImage('red.png',-1)
        self.greenLight, self.greenLight_rect = loadImage('green.png', -1)
        self.image, self.rect = (self.greenLight, self.greenLight_rect)
        self.inBoundries = True
        screen = pygame.display.get_surface()
        self.area = screen.get_rect()
        self.rect = self.image.get_rect(centerx = self.area.w * 0.5,
                     centery = self.area.h * 0.50)

    def update(self, time_passed) :
        if self.inBoundries:
            self.image = self.greenLight
        else:
            self.image = self.redLight
    
    def set_inBoundries(self, isit):
        self.inBoundries = isit
