#NBack
# import pygame._view
from pygame import *
import pygame.font
import pygame, sys, os
import random
from triallog import trialLog

class NBack(pygame.sprite.Sprite):
    def __init__(self, subjectNo, sessionNo, nback=1, trials=80, iti=0.8, freq=0.3):
        pygame.sprite.Sprite.__init__(self)
        
        #fontName=pygame.font.get_default_font()
        #self.font = font = pygame.font.SysFont(fontName, 72)
        fontpath = pygame.font.match_font('arial')
        self.font = pygame.font.Font(fontpath,82)
        self.image = self.font.render ('+' , 1, (255,255,255))
        screen = pygame.display.get_surface()
        self.rect = self.image.get_rect(
            centerx = screen.get_width() * 0.5,
            centery = screen.get_height() * 0.70)
        self.nback = nback
        self.frequency = freq #one probe each (freq) seconds
        self.timer = 0
        self.probe  =  [0,0]
        self.trial = [0,0,False]
        self.display = 'probe'
        self.iti = iti
        self.fixationSymbol = '+'
        filename = 'nback_%s_%s.json'%(subjectNo,sessionNo)
        i = 0
        while os.path.exists(filename):
            i+=1
            filename = 'nback_%s_%s(%s).json'%(subjectNo,sessionNo,i)
            print(filename)
        self.logme = trialLog(filename)
        self.totalTrials = trials
        self.subjectNo = subjectNo
        self.sessionNo = sessionNo

    def _probe(self):
        self.probe[0] = self.probe[1]
        while self.probe[0] == self.probe[1]:
            self.probe[1] = random.randrange(1,4,1)

        self.image = self.font.render ('%s'%self.probe[1] , 1, (255,255,255))
 
        
    def _fixation(self):
        self.image = self.font.render ( self.fixationSymbol, 1, (0,0,0))
        
    def update(self, time_passed):
        self.timer += (time_passed / 1000.0)
        if self.display == 'fixation':
            if self.timer >= self.frequency:
                if self.trial[2] == True:
                    self.logme.write(
                        subject = self.subjectNo,
                        session = self.sessionNo,
                        trialNumber = self.trial[0],
                        trialTime = self.trial[1],
                        RT = 0,
                        ACC = 'MISS')
                self._probe()
                self.trial[0] += 1
                if self.trial[0] > self.totalTrials:
                    pygame.quit()
                    sys.exit()
                self.trial[1] = pygame.time.get_ticks()
                self.trial[2] = True
                self.timer = 0
                self.display = 'probe'
                
        elif self.display == 'probe':
            if self.timer >= self.iti:
                self._fixation()
                self.timer = 0
                self.display = 'fixation'
            
        
    def response (self, number):
        if self.trial[2]:
            self.trial[2] = False
            if int(number) == self.probe[-1 * (self.nback + 1)]:
                self.logme.write(
                    subject = self.subjectNo,
                    session = self.sessionNo,
                    trialNumber = self.trial[0],
                    trialTime = self.trial[1],
                    RT = pygame.time.get_ticks() - self.trial[1],
                    ACC = 1)
            else:
                self.logme.write(
                    subject = self.subjectNo,
                    session = self.sessionNo,
                    trialNumber = self.trial[0],
                    trialTime = self.trial[1],
                    RT = pygame.time.get_ticks() - self.trial[1],
                    ACC = 0)
        else: 
            self.logme.write(
                subject = self.subjectNo,
                session = self.sessionNo,
                trialNumber = self.trial[0],
                trialTime = self.trial[1],
                RT = pygame.time.get_ticks() - self.trial[1],
                ACC = 1)            
        
        
        
        
        
    
