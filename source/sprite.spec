# -*- mode: python -*-
a = Analysis(['sprite.py'],
             pathex=['ui', 'D:\\Nir\\Documents\\CogBattary\\dev\\dual task\\pysorce'],
             hiddenimports=[],
             hookspath=None,
             runtime_hooks=None)
pyz = PYZ(a.pure)
exe = EXE(pyz,
          a.scripts,
          exclude_binaries=True,
          name='sprite.exe',
          debug=False,
          strip=None,
          upx=True,
          console=False )
coll = COLLECT(exe,
               a.binaries,
               a.zipfiles,
               a.datas,
               strip=None,
               upx=True,
               name='sprite')
