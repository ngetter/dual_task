﻿# import pygame._view
import pygame, sys, time
from pygame.locals import *
import random
from pygame import *

from signallight import *       
from plane import Plane
from nback import NBack

from hebtext import hebr, messages

def main(subjectNo=0, sessionNo=3):
    pygame.init()
    screen= pygame.display.set_mode((1024,768),pygame.FULLSCREEN | pygame.HWSURFACE | pygame.DOUBLEBUF)
    pygame.display.set_caption('Dual task')
    pygame.mouse.set_visible(False)
    
    background = pygame.Surface(screen.get_size())
    background = background.convert()
    background.fill ((0,0,0))

    centerx = background.get_width()/2
    centery = background.get_height()/2

    myfont = pygame.font.SysFont("monospace", 28)

    # render instructions
    label = myfont.render(hebr(messages['pressAnyKey']), 1, (255,255,255))
    textpos = label.get_rect()
    textpos.centerx = background.get_rect().centerx
    textpos.centery = background.get_height() * 0.85
    screen.blit(label, textpos)
    
    jj = 0.03
    for hebtext in messages['instructions'][sessionNo - 1]:
        jj += 0.07
        label = myfont.render(hebr(hebtext),1, (255,255,255))
        textpos = label.get_rect()
        textpos.centerx = background.get_rect().centerx
        textpos.centery = background.get_height() * jj
        screen.blit(label, textpos)
    
    pygame.display.flip()

    #clock = pygame.time.Clock()

    stall = True
    while stall:
        for event in pygame.event.get():
            if event.type == KEYDOWN:
                if sessionNo == 1:
                    plane = Plane(subjectNo,sessionNo)
                    sprite = pygame.sprite.RenderPlain(plane)
                elif sessionNo == 2:
                    nback = NBack(subjectNo,sessionNo,nback=1, trials=80, iti=0.8, freq=0.3)
                    sprite = pygame.sprite.RenderPlain(nback)
                elif sessionNo == 3:
                    plane = Plane(subjectNo,sessionNo)
                    nback = NBack(subjectNo,sessionNo,nback=1, trials=80, iti=0.8, freq=0.3)
                    sprite = pygame.sprite.RenderPlain(plane,  nback)
                else:
                    print('****************************\nERROR: session should be 1-3')
                    pygame.quit()
                    sys.exit()
                stall = False
                
    pygame.draw.line(background, (255,255,255),
                     (background.get_width() * 0.20, background.get_height()*0.40),
                     (background.get_width() * 0.80, background.get_height()*0.40), 1) 
    pygame.draw.line(background, (255,255,255),
                     (centerx, background.get_height()*0.40 - 5),
                     (centerx, background.get_height()*0.40 + 5), 1)   

    screen.blit(background, (0,0))
    pygame.display.flip()
    
    clock = pygame.time.Clock()
    frame_count = 0
    probe = 0
    while True:
        time_passed = clock.tick(100)
        frame_count += 1       

        for event in pygame.event.get():
            if event.type == QUIT:
                pygame.quit()
                sys.exit()
            elif event.type == MOUSEBUTTONDOWN:
                plane.hit()
            elif event.type == KEYDOWN:
                if event.key == K_LEFT:
                    plane.turn('left')
                elif event.key == K_RIGHT:
                    plane.turn('right')
                elif event.key == K_ESCAPE:
                    pygame.quit()
                    sys.exit()
                elif event.key >= K_1 and event.key <= K_4:
                    nback.response(pygame.key.name(event.key))

        sprite.update(time_passed)
        screen.blit(background, (0,0))
        sprite.draw(screen)
        pygame.display.flip()
        

if __name__ == '__main__':
    #dum = input('press enter key to start session')
    print('starting game')
    print(sys.argv)
    if len(sys.argv) == 3:
        session = int(sys.argv[1])
        subjNo = str(sys.argv[2])
    elif len(sys.argv) > 1:
        session = int(sys.argv[1])
        subjNo = '00'
    else:
        session = 3
        subjNo = '00'
    # sys.stderr = open('err.txt','w')
    # sys.stdout = open('log.txt','w')
    # print('{} {}').format(subjNo, session)
    # print(f'subject: {subjNo}, session: {session}')
    main(subjNo, session)
